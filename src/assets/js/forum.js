let editBtn = document.getElementById("editUserInfo")
let userName = document.getElementById("userName");
let userEmail = document.getElementById("userEmail");
let nameInput = document.getElementById("nameInput");
let lastNameInput = document.getElementById("lastNameInput");
let emailInput = document.getElementById("emailInput");
let passWordInput = document.getElementById("passWordInput");
let postComment = document.getElementById("postComment");

let userData = JSON.parse(localStorage.getItem('userData'));

postComment.onclick=(event)=>{
    event.preventDefault();
    obj = {
        id:userData[0].id,
        userName:userData[0].firstname+' '+userData[0].lastname,
        content:document.getElementById("commentText").innerText
    }
    console.log(obj);
    fetch('http://123.56.173.249:3000/api/postEvaluation', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(obj)
    })
        .then(response => response.json())
        .then(data => {
            clearComments()
            getEvaluationList()
            alert("send success!")
        })
        .catch(error => {
            console.error('Error:', error);
        });
}


let editShow = false
editBtn.onclick = () => {
    if (!editShow) {

        userName.classList.add("hidenUser");
        userEmail.classList.add("hidenUser");
        nameInput.classList.remove("hidenUser");
        lastNameInput.classList.remove("hidenUser");
        emailInput.classList.add("showInput");
        passWordInput.classList.add("showInput");

        nameInput.value = userData[0].firstname
        lastNameInput.value = userData[0].lastname
        emailInput.value = userData[0].email
        passWordInput.value = userData[0].password
    } else {
        userName.classList.remove("hidenUser");
        userEmail.classList.remove("hidenUser");
        nameInput.classList.add("hidenUser");
        lastNameInput.classList.add("hidenUser");
        emailInput.classList.remove("showInput");
        passWordInput.classList.remove("showInput");

        const obj = {
            email: document.getElementById("emailInput").value,
            password: document.getElementById("passWordInput").value,
            firstname: document.getElementById("nameInput").value,
            lastname: document.getElementById("lastNameInput").value,
            id: "1"//暂时固定
        };
        fetch('http://123.56.173.249:3000/updateUser', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(obj)
        })
            .then(response => response.json())
            .then(data => {
                userName.innerText = obj.firstname + ' ' + obj.lastname
                userEmail.innerText = "Email: " + obj[0].email
                alert("update success!")
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }

    editShow = !editShow
}

userName.innerText = userData[0].firstname + ' ' + userData[0].lastname
userEmail.innerText = "Email: " + userData[0].email;

function clearComments() {
    const commentElements = document.querySelectorAll('.comment');
    commentElements.forEach(comment => {
        comment.remove();
    });
}

function getEvaluationList() {
    fetch('http://123.56.173.249:3000/api/evaluationList')
        .then(response => response.json())
        .then(data => {
            data.forEach(data => {
                const commentElement = document.createElement('div');
                commentElement.classList.add('comment');
                const commentContent = document.createElement('p');
                commentContent.innerHTML = `<strong>${data.userName}:</strong> ${data.content}`;
                commentElement.appendChild(commentContent);
                const commentBox = document.querySelector('.comment-box');
                commentBox.appendChild(commentElement);
            });
        })
        .catch(error => {
            console.error('Error:', error);
        });
};
getEvaluationList()
