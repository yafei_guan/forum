import Vue from "vue";
import Router from "vue-router";
import Publish from "@/components/publish";
import Forum from "@/components/forum";
import Dashboard from "@/components/dashboard";
import Login from "@/components/login";
import Question from "@/components/question";

Vue.use(Router);

export default new Router({
  mode: "hash",
  routes: [
    {
      path: "/login",
      component: Login,
    },
    {
      path: "/dashboard",
      component: Dashboard,
    },
    {
      path: "/question/:questionId",
      name: "Question",
      component: Question,
    },
    {
      path: "/forum",
      component: Forum,
    },
    {
      path: "/publish/add",
      component: Publish,
    },
    {
      path: '/publish/:questionId',
      name: 'Publish',
      component: Publish,
    },
  ],
});
