let menu = document.querySelector('#menu-btn');
let navbar = document.querySelector('.header .navbar');

menu.onclick = () => {
    menu.classList.toggle('fa-times');
    navbar.classList.toggle('active');
};

window.onscroll = () => {
    menu.classList.remove('fa-times');
    navbar.classList.remove('active');
};


var swiper = new Swiper(".home-slider", {
    spaceBetween: 20,
    effect: "fade",
    grabCursor: true,
    loop: true,
    centeredSlides: true,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
});

var swiper = new Swiper(".review-slider", {
    spaceBetween: 20,
    grabCursor: true,
    loop: true,
    autoplay: {
        delay: 7500,
        disableOnInteraction: false,
    },
    breakpoints: {
        0: {
            slidesPerView: 1,
        },
        600: {
            slidesPerView: 2,
        },
    },
});

var swiper = new Swiper(".blogs-slider", {
    spaceBetween: 20,
    grabCursor: true,
    loop: true,
    autoplay: {
        delay: 7500,
        disableOnInteraction: false,
    },
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    breakpoints: {
        0: {
            slidesPerView: 1,
        },
        768: {
            slidesPerView: 2,
        },
        991: {
            slidesPerView: 3,
        },
    },
});

const handleEditButtonClick = (index) => {
    const modal = document.getElementById('editModal');
    const span = document.getElementsByClassName('close')[0];
    const editForm = document.getElementById('editForm');

    fetch('http://123.56.173.249:3000/api/getTeachers')
        .then(response => response.json())
        .then(data => {
            const instructorSelect = document.getElementById('instructorSelect');
            instructorSelect.innerHTML = '';
            data.forEach(teacher => {
                const option = document.createElement('option');
                option.value = teacher.id;
                option.textContent = teacher.name;
                instructorSelect.appendChild(option);
            });

        })
        .catch(error => {
            console.error('Error:', error);
        });


    modal.style.display = 'block';

    span.onclick = function () {
        if (confirm(`
        Are you sure you want to close the reservation page? If the amount is closed, it will be automatically returned to the account`)) {
            modal.style.display = 'none';
        }
    }
    
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = 'none';
        }
    }

    editForm.onsubmit = function (event) {
        event.preventDefault();
        const instructor = document.getElementById('instructorSelect').value;
        const date = document.getElementById('dateSelect').value;
        const time = document.getElementById('timeSelect').value;
        const userData = JSON.parse(localStorage.getItem('userData')); 

        const obj = {
            teacherName: instructor,
            time: date + ' ' + time,
            userName: userData[0].firstname+' '+userData[0].lastname,
            isOver: 0
        };
        fetch('http://123.56.173.249:3000/api/appointment', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(obj)
        })
            .then(response => response.json())
            .then(data => {
                if (data.status == 200) {
                    modal.style.display = 'none';
                }
            })
            .catch(error => {
                console.error('Error:', error);
            });

    }
}



const orderSureModal=(price)=>{
    let makesureOrder = document.getElementById("makesureOrder")
    const span = document.getElementsByClassName('closeOrder')[0];
    makesureOrder.style.display="block"
    let priceNum = document.getElementById("priceNum")
    priceNum.innerText=price

    span.onclick = function () {
        makesureOrder.style.display = 'none';
    }
    window.onclick = function (event) {
        if (event.target == makesureOrder) {
            makesureOrder.style.display = 'none';
        }
    }


}



let silverClick = document.getElementById('silverClick')
let goldClick = document.getElementById('goldClick')
let diamondClick = document.getElementById('diamondClick')

silverClick.onclick = () => {
    orderSureModal("30")
    // handleEditButtonClick('1')
}
goldClick.onclick = () => {
    orderSureModal("90")
    // handleEditButtonClick('2')
}
diamondClick.onclick = () => {
    orderSureModal("150")
    // handleEditButtonClick('3')
}


let UserInfoA = document.getElementById('UserInfo')
UserInfoA.onclick = () => {
    const modal = document.getElementById('userInfoModal');
    const span = document.getElementsByClassName('closeUserInfo')[0];
    modal.style.display = 'block';

    const userModalName = document.getElementById('userModalName');
    const userModalemail = document.getElementById('userModalemail');
    const userData = JSON.parse(localStorage.getItem('userData')); 
    userModalName.innerText = userData[0].firstname+' '+userData[0].lastname;
    userModalemail.innerText="Email: "+userData[0].email

    span.onclick = function () {
        modal.style.display = 'none';
    }
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = 'none';
        }
    }
    let obj={
        userName:userData[0].firstname+' '+userData[0].lastname
    }
    fetch('http://123.56.173.249:3000/api/myAppointmentList', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(obj)
    })
    .then(response => response.json())
    .then(data => {
        const appointmentList = document.querySelector('.appointment-list'); // 修改选择器以匹配 HTML 中的类名
        appointmentList.innerHTML = "";

        if (data && data.length > 0) {
            data.forEach(appointment => {
                const appointmentItem = document.createElement('li');
                appointmentItem.textContent = "teacher:"+appointment.teacherName+"---time:"+appointment.time;
                appointmentList.appendChild(appointmentItem);
            });
        } else {
            // 如果没有预约记录，显示相应信息
            appointmentList.innerHTML += "<p style='color:#fff'>No appointment records found.</p>";
        }
    })
    .catch(error => {
        console.error('Error:', error);
    });
    

}

let orderSureBtn = document.getElementById("orderSureBtn")

orderSureBtn.onclick=()=>{
    alert("payment successful!")
    let makesureOrder = document.getElementById("makesureOrder")
    makesureOrder.style.display="none"
    handleEditButtonClick("1")
}

let goHome = document.getElementById("goHome")

goHome.onclick=()=>{
    window.location.href = window.location.origin + '/article/editor'
}